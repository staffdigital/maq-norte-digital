$(function(){
	$('.soloNumber').each((k, el)=>{
		$(el).on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
	function cerrar_nav() {
		$('.header__sidebar--open').removeClass('active');
		$('.header__sidebar--close').removeClass('active');
		$('.header__sidebar').removeClass('active');
		$('.header__sidebar__overlay').removeClass('active');
		$('body').removeClass('active');
	};

	function abrir_nav(){
		$('.header__sidebar--open').addClass('active');
		$('.header__sidebar--close').addClass('active');
		$('.header__sidebar').addClass('active');
		$('.header__sidebar__overlay').addClass('active');
		$('body').addClass('active');
	}

	$('.header__sidebar--close , .header__sidebar__overlay').click(function(event) {
		event.preventDefault();
		cerrar_nav();
	});	

	$('.header__sidebar--open').click(function(event) {
		abrir_nav()
	});

	//detectando tablet, celular o ipad
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	
	// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		// tasks to do if it is a Mobile Device
		function readDeviceOrientation() {
			if (Math.abs(window.orientation) === 90) {
				// Landscape
				cerrar_nav();
			} else {
				// Portrait
				cerrar_nav();
			}
		}
		window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function() {
			var estadomenu = $('.menu-responsive').width();
			if(estadomenu != 0){
				cerrar_nav();
			}
		});
	}
	

	// header scroll
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		if (altoScroll > 0) {
			$('.header__fixed').addClass('scrolling');
		}else{
			$('.header__fixed').removeClass('scrolling');
		};
	});


	//focus para el input
	$('.general__input input, .general__input textarea, .general__input select, .general__input label').focusin(function() {
		$(this).parent().parent().addClass('active');
	});

	$('.general__input input, .general__input textarea, .general__input select, .general__input label').focusout(function() {
		if ($(this).val() === "") {
			$(this).parent().parent().removeClass('active');
		};
	});

	$('.general__input input, .general__input textarea, .general__input select, .general__input label').each(function() {
		if ($(this).val() != "") {
			$(this).parent().parent().addClass('active');
		};
	});
});
